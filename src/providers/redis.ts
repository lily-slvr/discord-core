import type Bot from "../main";
import { BaseProvider, Provider } from "./baseProvider";

interface redisOpts {}

export default class redisProvider extends BaseProvider implements Provider<redisOpts> {
  constructor(bot: Bot, _opts: redisOpts) {
    super(bot, "redis");
  }
}
