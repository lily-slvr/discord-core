import type Bot from "../main";

export interface Provider<_> extends BaseProvider {
  bot: Bot;
  id: string;
}

// to allow the class definition itself to be used as a parameter
export interface BaseProviderDefinition<t> {
  new (bot: Bot, args: t): Provider<t>;
}

/**
 * Base provider class, all providers should extend this class
 * @class BaseProvider
 * @param {Bot} bot - BotCore instance
 * @param {string} id - the provider's id
 * @function provide - returns the provider's instance
 */
export class BaseProvider {
  bot: Bot;
  id: string;
  constructor(bot: Bot, id: string) {
    this.bot = bot;
    this.id = id;
  }
}
