import { MongoClient, MongoClientOptions } from "mongodb";
import type Bot from "../main";
import { BaseProvider, Provider } from "./baseProvider";

/**
 *
 */
interface mongoOpts {
  uri: string;
  database?: string;
  collection?: string;
  passthrough?: MongoClientOptions;
  errorHandler?: (err: Error) => void;
}

/**
 * Provides a MongoDB client
 * @class MongoProvider
 * @param {Bot} bot - BotCore instance
 * @param {string} uri - the mongodb server connection string
 * @param {MongoClientOptions} opts.passthrough - options to pass to the mongodb client
 * @param {(err: Error) => void} opts.errorHandler - function to pass errors to
 * @param {string} opts.database - the default database to use
 * @param {string} opts.collection - the default collection to use
 * @function provide - returns the provider's instance
 * @extends BaseProvider
 */
export default class MongoProvider extends BaseProvider implements Provider<mongoOpts> {
  mongo: MongoClient;
  errorHandler: (err: Error) => void;
  database: string;
  collection: string;
  constructor(bot: Bot, opts: mongoOpts) {
    super(bot, "mongo");
    this.bot = bot;
    this.id = "mongo";
    this.mongo = new MongoClient(opts.uri, opts.passthrough);
    this.database = opts.database ?? process.env["PROJECT_NAME"] ?? "slvrcore-unnamed";
    this.collection = opts.collection ?? `${process.env["PROJECT_NAME"]}-data` ?? "data";
    this.errorHandler =
      opts.errorHandler ??
      ((err) => {
        console.log(
          `MongoDB Provider Error: \n(if you would like to supress this message provide your own error handler function) \n${err}`
        );
      });

    this.mongo.connect().catch((err) => {
      this.errorHandler(err);
    });
  }

  provide(): MongoClient {
    return this.mongo;
  }

  /**
   * stores data in the database
   * @param data - the data to insert
   * @param collection - the collection to insert into
   * @param database - the database to insert into
   */
  store(data: any, collection?: string | undefined, database?: string) {
    this.mongo
      .db(database ?? this.database)
      .collection(collection ?? this.collection)
      .insertOne(data)
      .catch((err) => {
        this.errorHandler(err);
      });
  }
}
