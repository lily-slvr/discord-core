import type { BitFieldResolvable, IntentsString } from "discord.js";
import { Client } from "discord.js";
import { BaseModuleDefinition, BaseModule, Module } from "./modules/baseModule";

import mongoProvider from "./providers/mongo";
import redisProvider from "./providers/redis";

import modals from "./modules/modals";
import commands from "./modules/commands";
import type { BaseProviderDefinition, Provider } from "./providers/baseProvider";

/**
 * LilysBotCore
 * allows modular discord.js bot creation
 */
export default class LilysBotCore {
  public client: Client;
  private modules!: { modals: modals; commands: commands; [key: string]: Module<any> };
  private providers!: { mongo: mongoProvider; redis: redisProvider; [key: string]: Provider<any> };
  private token: string;

  /**
   * @param opts.token - the bot's token
   * @param opts.intents - the intents to use
   * @param opts.mongoUrl - mongo DB connection string
   * @param opts.redisUrl - redis DB connection string
   */
  constructor(opts: {
    token: string;
    intents: BitFieldResolvable<IntentsString, number>;
    mongoUrl: string;
    redisUrl: string;
  }) {
    this.client = new Client({ intents: opts.intents });
    this.token = opts.token;

    // register default providers
    (this as any).providers = {};
    this.registerProvider(mongoProvider, { uri: "localhost" });
    this.registerProvider(redisProvider, {});

    // register default modules
    (this as any).modules = {};
    this.registerModule(modals);
    this.registerModule(commands);
  }

  /**
   * connect to discord
   */
  public connect() {
    this.client.on("ready", () => {
      console.log("Client connected");
    });

    this.client.login(this.token);
  }

  /**
   * register a provider
   * the providers constructor is passed the bot instance as its first parameter and the args object (if provided) as its second
   * @param providerid - the provider's id
   * @param provider - the service provider
   */
  public registerProvider<t>(Provider: BaseProviderDefinition<t>, args: t) {
    let provider = new Provider(this, args);
    console.log(`registering provider ${provider.id}`);
    this.providers[provider.id] = provider;
  }

  /**
   * registers a module
   * the modules constructor is passed the bot instance as its first parameter and the args object (if provided) as its second
   * @param moduleid - the module's id
   * @param module - the module class definition, must extend BaseModule
   */
  public registerModule<t>(Module: BaseModuleDefinition<t>, args?: any) {
    console.log(`registering module ${module.id}`);
    this.modules[module.id] = new Module(this, args);
  }

  /**
   * get a provider
   * @param providerid - the provider's id
   * @returns the provider's instance, returns undefined if the provider doesn't exist
   */
  public getProvider<t>(providerid: string): Provider<t> | undefined {
    return this.providers[providerid];
  }

  /**
   * get a module
   * @param moduleid - the module's id
   * @returns the module's instance, returns undefined if the module doesn't exist
   */
  public getModule<t>(moduleid: string): Module<t> | undefined {
    return this.modules[moduleid];
  }
}

export { LilysBotCore, BaseModule };
