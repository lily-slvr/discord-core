import type { LilysBotCore } from "../main";
import { BaseModule } from "./baseModule";

export default class Commands extends BaseModule {
  constructor(bot: LilysBotCore) {
    super(bot, "commands");
  }
}
