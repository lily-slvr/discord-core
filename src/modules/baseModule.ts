import type Bot from "../main";

export interface Module<_> extends BaseModule {}

// to allow the class definition itself to be used as a parameter
export interface BaseModuleDefinition<t> {
  new (bot: Bot, args: t): Module<t>;
}

// base module class, all modules should extend this class
export class BaseModule {
  bot: Bot;
  id: string;
  constructor(bot: Bot, id: string) {
    this.bot = bot;
    this.id = id;
  }
}
